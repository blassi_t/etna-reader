package main

import (
	"log"
	"os"
	"sync"
	"wallreader/etnac"
)

func main() {
	etna, err := etnac.Authenticate(etnac.Credentials{
		Login:    os.Getenv("ETNA_USER"),
		Password: os.Getenv("ETNA_PASS"),
	})
	if err != nil {
		log.Fatalln(err)
	}

	for _, wall := range []string{
		"Etna - Infos",
		"Etna - Événements",
		"Etna - Café",
		"Etna - Petites Annonces",
		"Etna - Jobs",
		"Etna - Questions Techniques",
	} {
		for {
			conversations, err := etna.GetConversations(wall)
			if err != nil {
				log.Fatalln(err)
			}
			if conversations.Total == 0 {
				break
			}
			for _, conv := range conversations.Hits {
				messages, err := etna.GetMessages(conv)
				if err != nil {
					log.Fatalln(err)
				}
				wg := sync.WaitGroup{}
				for _, message := range messages {
					wg.Add(1)
					go etna.ViewMessage(&wg, conv, message)
				}
				wg.Wait()
			}
		}
	}
}
