package etnac

type WallMessageView struct {
	User      int    `json:"user"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type WallMessage struct {
	ID        int               `json:"id"`
	User      int               `json:"user"`
	Content   string            `json:"content"`
	Type      string            `json:"type"`
	CreatedAt string            `json:"created_at"`
	UpdatedAt string            `json:"updated_at"`
	Views     []WallMessageView `json:"views"`
}

type WallLastMessage struct {
	ID             int    `json:"id"`
	User           int    `json:"user"`
	Content        string `json:"content"`
	Type           string `json:"type"`
	CreatedAt      string `json:"created_at"`
	UpdatedAt      string `json:"updated_at"`
	ConversationID int    `json:"conversation_id"`
}

type WallConversation struct {
	ID          int             `json:"id"`
	Title       string          `json:"title"`
	ACLs        []string        `json:"acls"`
	LastMessage WallLastMessage `json:"last_message"`
	CreatedAt   string          `json:"created_at"`
	UpdatedAt   string          `json:"updated_at"`
	DeletedAt   string          `json:"deleted_at"`
}
type WallConversations struct {
	Total int                `json:"total"`
	Hits  []WallConversation `json:"hits"`
	From  int                `json:"from"`
	Size  int                `json:"size"`
}
