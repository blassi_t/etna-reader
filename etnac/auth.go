package etnac

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
)

type Credentials struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type User struct {
	ID        int      `json:"id"`
	Login     string   `json:"login"`
	Email     string   `json:"email"`
	LogAs     bool     `json:"logas"`
	Groups    []string `json:"groups"`
	LoginDate string   `json:"login_date"`
}
type EtnaClient struct {
	client *http.Client
	user   User
}

func Authenticate(credentials Credentials) (*EtnaClient, error) {
	etnac := &EtnaClient{}

	jar, err := cookiejar.New(nil)
	if err != nil {
		return nil, err
	}
	body, err := json.Marshal(credentials)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, "https://auth.etna-alternance.net/identity", bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json;charset=UTF-8")

	etnac.client = &http.Client{
		Jar: jar,
	}
	resp, err := etnac.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatalln(err.Error())
		}
	}(resp.Body)

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(content, &etnac.user)
	if err != nil {
		return nil, err
	}
	return etnac, nil
}
