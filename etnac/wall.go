package etnac

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"sync"
)

func (c *EtnaClient) GetConversations(wall string) (*WallConversations, error) {
	req, err := http.NewRequest(http.MethodGet, "https://intra-api.etna-alternance.net", nil)
	if err != nil {
		return nil, err
	}
	req.URL.Path = fmt.Sprintf("/walls/%s/conversations", wall)
	req.URL.RawQuery = "from=0&q=+%2Bcreated_at:%5B*+TO+*%5D+-last_message.views:" + strconv.Itoa(c.user.ID) + "&size=100"
	fmt.Println(req.URL.String())
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	data := &WallConversations{}
	if err = json.Unmarshal(content, data); err != nil {
		return nil, err
	}
	return data, nil
}

func (c *EtnaClient) GetMessages(conv WallConversation) ([]WallMessage, error) {
	req, err := http.NewRequest(http.MethodGet, "https://intra-api.etna-alternance.net", nil)
	if err != nil {
		return nil, err
	}
	req.URL.Path = fmt.Sprintf("/conversations/%d/messages", conv.ID)
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var data []WallMessage
	if err = json.Unmarshal(content, &data); err != nil {
		return nil, err
	}
	return data, nil
}

func (c *EtnaClient) ViewMessage(wg *sync.WaitGroup, conv WallConversation, message WallMessage) {
	defer wg.Done()
	type Message struct {
		ConversationID int `json:"conversation_id"`
		MessageID      int `json:"message_id"`
	}

	data, _ := json.Marshal(Message{
		ConversationID: conv.ID,
		MessageID:      message.ID,
	})
	req, err := http.NewRequest(http.MethodPut, "https://intra-api.etna-alternance.net", bytes.NewBuffer(data))
	if err != nil {
		return
	}
	req.URL.Path = fmt.Sprintf("/conversations/%d/messages/%d/view", conv.ID, message.ID)
	resp, err := c.client.Do(req)
	if err != nil {
		return
	}
	fmt.Println(resp.StatusCode)
}
